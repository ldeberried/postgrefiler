import shutil
import os.path
import typer
import re

import postgrefiler.query_constant as queries
import postgrefiler.file_constant as filenames


def delete_from_dir(directory):
    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            typer.echo(typer.style('Failed to delete %s. Reason: %s' % (file_path, e), fg=typer.colors.RED))


def get_tables(db_connection):
    query = '''
        SELECT table_name 
        FROM information_schema.tables
        WHERE table_schema NOT IN ('pg_catalog', 'information_schema')
    '''
    connection_cursor = db_connection.cursor()
    connection_cursor.execute(query)
    returnable_data = connection_cursor.fetchall()
    connection_cursor.close()
    return returnable_data


def get_columns(db_connection, table):
    query = '''
        SELECT column_name, udt_name, is_nullable, character_maximum_length, column_default
        FROM information_schema.columns
        WHERE table_name = %s
        ORDER BY ordinal_position
    '''
    connection_cursor = db_connection.cursor()
    connection_cursor.execute(query, table)
    returnable_data = connection_cursor.fetchall()
    connection_cursor.close()
    return returnable_data


def write_table_script(table_columns, table, file_path):
    table_name = table[0]
    name_of_file = table_name + '_create'
    file_path = file_path + table_name + '\\'
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    complete_name = os.path.join(file_path, name_of_file + ".sql")

    column_list = []
    for column in table_columns:

        data_type = column[1]
        if column[1] == 'int8' and column[4] == "nextval('" + table_name + "_" + column[0] + "_seq'::regclass)":
            data_type = 'bigserial'
        elif re.search('^(_)', column[1]):
            data_type = column[1].replace('_', '') + '[]'

        result = '''%s %s %s %s''' % (
            column[0], data_type + '(' + str(column[3]) + ')' if data_type == 'varchar' else data_type,
            'NULL' if column[2] == 'YES' else 'NOT NULL',
            '' if column[4] is None or column[4] == "nextval('" + table_name + "_" + column[
                0] + "_seq'::regclass)" else 'default ' + str(column[4]))
        print(result)

        column_list.append(result)

    table_creation_script = queries.DROP_CREATE_TABLE % (table_name, table_name, ',\n'.join(column_list))

    with open(complete_name, "w", encoding="utf-8") as file:
        try:
            file.write(table_creation_script)
            return True
        except:
            return False


def get_data_from_table(db_connection, table, file_path):
    name_of_file = table[0] + '_data'
    file_path = file_path + table[0] + '\\'
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    completeName = os.path.join(file_path, name_of_file + ".csv")
    with open(completeName, "w", encoding="utf-8") as csv_file:
        with db_connection.cursor() as connection_cursor:
            try:
                query = 'SELECT * FROM %s' % (table[0])

                sql_for_file_output = "COPY ({0}) TO STDOUT WITH NULL AS 'null' CSV HEADER DELIMITER ','".format(query)
                connection_cursor.copy_expert(sql_for_file_output, csv_file)
                return True
            except:
                return False


def get_stored_procedures(db_connection):
    query = '''
        SELECT
            pg_get_functiondef(p.oid)
        FROM
            pg_catalog.pg_namespace n
        JOIN
            pg_catalog.pg_proc p ON p.pronamespace = n.oid
        WHERE n.nspname = 'public'
    '''
    connection_cursor = db_connection.cursor()
    connection_cursor.execute(query)
    returnable_data = connection_cursor.fetchall()
    connection_cursor.close()
    return returnable_data


def write_stored_procedures(stored_procedures, file_path):
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    complete_name = os.path.join(file_path, filenames.STORED_FUNCTIONS_FILE)
    file = open(complete_name, "w", encoding="utf-8")
    try:
        for sp in stored_procedures:
            file.write(sp[0])
            file.write(';\n')
        file.close()
        typer.echo(typer.style('Stored Procedures extracted from schema succesfully! 🥳', fg=typer.colors.GREEN))
    except:
        file.close()
        typer.echo(typer.style("Unable to extract stored  procedures. Exiting...", fg=typer.colors.RED))
        typer.Exit()
        delete_from_dir(directory=file_path)


def get_constraints(db_connection):
    query = '''
        select 'ALTER TABLE '|| relname || ' ADD CONSTRAINT ' || conname || ' ' ||  pg_get_constraintdef(p.oid)  
        || ';' as condef
        from pg_constraint  p
            join pg_catalog.pg_class c on p.conrelid = c.oid
        where connamespace <> 11
    '''
    connection_cursor = db_connection.cursor()
    connection_cursor.execute(query)
    returnable_data = connection_cursor.fetchall()
    connection_cursor.close()
    return returnable_data


def write_constraints(stored_procedures, file_path):
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    complete_name = os.path.join(file_path, filenames.CONSTRAINTS_FILE)
    with open(complete_name, "w", encoding="utf-8") as file:
        try:
            for function in stored_procedures:
                file.write(function[0])
                file.write('\n')
            typer.echo(typer.style('Constraints extracted from schema succesfully! 🥳', fg=typer.colors.GREEN))
        except:
            typer.echo(typer.style("Unable to extract constraints. Exiting...", fg=typer.colors.RED))
            typer.Exit()
            delete_from_dir(directory=file_path)
