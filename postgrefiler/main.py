from xmlrpc.client import Boolean
from typing import Optional

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from postgrefiler.getdb_functions import *
from postgrefiler.setdb_functions import *

import os

app = typer.Typer()


@app.callback()
def callback():
    '''
    From File system to PostgreSQL and viceversa!
    '''


@app.command()
def hello():
    print("Holi")


@app.command()
def getdb(dbname: str = typer.Argument(...), default: Boolean = True,
          host: Optional[str] = typer.Argument('localhost', help="host name if default connection is not used"),
          port: Optional[int] = typer.Argument(5432, help="port if default connection is not used"),
          user: Optional[str] = typer.Argument('postgres', help="user name if default connection is not used"),
          passwd: Optional[str] = typer.Argument('postgres', help="user's password if default connection is not used")):
    if default:
        connection_config = {
            'host': 'localhost',
            'port': 5432,
            'user': 'postgres',
            'passwd': 'postgres'
        }
    else:
        connection_config = {
            'host': host,
            'port': port,
            'user': user,
            'passwd': passwd
        }

    typer.echo('DB Manager with psycopg2 on 🐍')

    db_connection = psycopg2.connect(dbname=dbname, host=connection_config['host'], port=connection_config['port'],
                                     user=connection_config['user'], password=connection_config['passwd'])

    file_path = typer.prompt("Select directory to store scripts")
    file_path = file_path + '\\'

    process_succesful_data = True
    process_succesful_schema = True

    for table in get_tables(db_connection=db_connection):
        table_columns = get_columns(db_connection=db_connection, table=table)
        process_succesful_schema = write_table_script(table_columns, table=table, file_path=file_path)

        process_succesful_data = get_data_from_table(db_connection=db_connection, table=table, file_path=file_path)

    if (process_succesful_schema):
        typer.echo(typer.style('Schema extracted succesfully! 🥳', fg=typer.colors.GREEN))
    else:
        delete_from_dir(directory=file_path)
        typer.Exit()

    if (process_succesful_data):
        typer.echo(typer.style('Data extracted from schema succesfully! 🥳', fg=typer.colors.GREEN))
    else:
        delete_from_dir(directory=file_path)
        typer.Exit()

    sp_data = get_stored_procedures(db_connection=db_connection)
    write_stored_procedures(stored_procedures=sp_data, file_path=file_path)

    sp_data = get_constraints(db_connection=db_connection)
    write_constraints(stored_procedures=sp_data, file_path=file_path)


@app.command()
def setdb(dbname: str = typer.Argument(...), default: Boolean = True,
            host: Optional[str] = typer.Argument('localhost', help="host name if default connection is not used"),
            port: Optional[int] = typer.Argument(5432, help="port if default connection is not used"),
            user: Optional[str] = typer.Argument('postgres', help="user name if default connection is not used"),
            passwd: Optional[str] = typer.Argument('postgres',
                                                   help="user's password if default connection is not used")):
    if default:
        connection_config = {
            'host': 'localhost',
            'port': 5432,
            'user': 'postgres',
            'passwd': 'postgres'
        }
    else:
        connection_config = {
            'host': host,
            'port': port,
            'user': user,
            'passwd': passwd
        }

    typer.echo('DB Manager with psycopg2 on 🐍')

    db_connection = psycopg2.connect(dbname=dbname, host=connection_config['host'], port=connection_config['port'],
                                     user=connection_config['user'], password=connection_config['passwd'])
    db_connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    file_path = typer.prompt("Select directory to store scripts: ")
    file_path = file_path + '\\'

    access_file_system(file_path=file_path, db_connection=db_connection)


if __name__ == "__main__":
    app()
