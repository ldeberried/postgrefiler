import os.path
import psycopg2
import typer

import postgrefiler.query_constant as queries
import postgrefiler.file_constant as filenames

def access_file_system(file_path, db_connection):
    try:
        clean_schema(db_connection)
    except:
        typer.echo(typer.style("❌ Unable to drop schema. Exiting...", fg=typer.colors.RED))
        typer.Exit()

    with db_connection.cursor() as connection_cursor:
        for directory in os.listdir(file_path):
            iterated_dir = os.path.join(file_path, directory)
            if os.path.isdir(iterated_dir):
                with open(os.path.join(iterated_dir, directory + '_create.sql'), 'r', encoding='UTF-8') as sql_file:
                    connection_cursor.execute(sql_file.read())

                with open(os.path.join(iterated_dir, directory + '_data.csv'), 'r', encoding='UTF-8') as csv_file:
                    query = "COPY " + directory + " FROM STDIN WITH NULL AS 'null' CSV HEADER DELIMITER ','"
                    typer.echo(query)
                    connection_cursor.copy_expert(query, csv_file)

        if filenames.CONSTRAINTS_FILE in os.listdir(file_path):
            with open(os.path.join(file_path, filenames.CONSTRAINTS_FILE), 'r', encoding='UTF-8') as sql_file:
                connection_cursor.execute(sql_file.read())

        if filenames.STORED_FUNCTIONS_FILE in os.listdir(file_path):
            with open(os.path.join(file_path, filenames.STORED_FUNCTIONS_FILE), 'r', encoding='UTF-8') as sql_file:
                connection_cursor.execute(sql_file.read())



def clean_schema(db_connection: psycopg2.connect):
    schema = typer.prompt('Select schema to be drop and restored: ')

    with db_connection.cursor() as connection_cursor:
        connection_cursor.execute(queries.DROP_SCHEMA % (schema, schema))
